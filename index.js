// console.log("Hello World!");

	let firstName = "John";
	let lastName = "Smith";
	let age = 30;
	let hobbies = ["Biking", "Mountain Climbing", "Swimming"];
	let workAddress = {
		houseNumber: 32,
		street: "Washington",
		city: "Lincoln",
		state: "Nebraska"
	}

	let isMarried = true;

	function printUserInfo(fName, lName, userAge, userHobbies, userWorkAdd){
		console.log(`First Name: ${fName}`);
		console.log(`Last Name: ${lName}`);
		console.log(`Age: ${userAge}`);

		console.log(`Hobbies:`);
		console.log(userHobbies);
		console.log(`Work Address:`);
		console.log(userWorkAdd);
	}

	printUserInfo(firstName, lastName, age, hobbies, workAddress);

	function returnFunction(fName, lName, userAge, userHobbies, userWorkAdd){

		console.log(`${fName} ${lName} is ${userAge} years of age.`);
		console.log(`This was printed inside of the function`);
		console.log(userHobbies);
		console.log(`This was printed inside of the function`);
		console.log(userWorkAdd);
		console.log(`The value of isMaried is: `, isMarried);
		
		return fName, lName, userAge, userHobbies, userWorkAdd
	}

	let userIntro = returnFunction(firstName, lastName, age, hobbies, workAddress);
	userIntro;